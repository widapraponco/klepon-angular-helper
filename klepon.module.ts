import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DynamicFormComponent, ModalComponent } from './components';


@NgModule({
  declarations: [
    DynamicFormComponent,
    ModalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers:[],
  exports: [
    DynamicFormComponent,
    ModalComponent,
  ]
})
export class KleponModule { }
