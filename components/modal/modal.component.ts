import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { QuestionControlService } from "../../services/question-control.service";
import { Observable } from 'rxjs';
import { QuestionBase } from '../../models';
declare var $: any;

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.scss"]
})
export class ModalComponent implements OnInit {
  @Input() id: string;
  @Input() questions: QuestionBase<any>[] = [];
  @Input() title: String = "Modal Title";
  @Output() callback: EventEmitter<any> = new EventEmitter();
  @Input() params: string;

  @ViewChild('modal', {static: true}) modal: ElementRef;

  form: FormGroup;
  payload = "";
  loading: boolean = false;
  isSuccess: boolean = false;
  isError: boolean = false;
  message: string = "";

  constructor(private qcs: QuestionControlService, public elementRef: ElementRef) {}

  ngOnInit() {
    this.isSuccess = false;
    this.isError = false;
    this.form = this.qcs.toFormGroup(this.questions);
  }

  show(title?: String) {
    this.title = title;
    $(this.elementRef.nativeElement).modal('show');
  }
  hide = () => $(this.elementRef.nativeElement).modal('hide');

  showWithQuestion(questions) {
    this.questions = questions;
    this.form = this.qcs.toFormGroup(this.questions);
    this.show();
  }

  onSubmit() {
    this.payload = this.form.getRawValue();
    //console.log(this.payload);

    if (this.callback) this.callback.emit(this.payload);
  }

  updateQuestion(questions: QuestionBase<any>[]) {
    this.questions = questions;
    this.form = this.qcs.toFormGroup(questions);
  }

  wait() {
    this.loading = true;
  }

  done(msg: string) {
    this.isSuccess = true;
    this.loading = false;
    this.message = `<ul><li>${msg}</li></ul>`;

    setInterval(() => {
      this.isSuccess = false;
      this.message = "";
    }, 10000);
    this.form = this.qcs.toFormGroup(this.questions);
  }

  error(err) {
    this.isError = true;
    this.loading = false;

    const errorKey: string = Object.keys(err.error)[0];
    let e = [];
    if (errorKey === 'errors') {
      for (var key in err.error[errorKey]) {
        e.push(err.error[errorKey][key]); 
      }
      this.message = `<ul>${e.map(e => `<li>${e}</li>`).join("")}</ul>`;
    } else {
      this.message = `<ul>${err.message}</ul>`;
    }

    setInterval(() => {
      this.isError = false;
      this.message = "";
    }, 10000);
  }

  async submit(req: Observable<any>, options?:{success?: string, error?: string, key?: string, custom?: {
    text: string, keys: string[]
  } }) {
    this.wait();
    req.subscribe(
      res => {
        this.done(options.success || 
          res.data[options.key] ||
          this.customResponse(options.custom, res) ||
          'Success');
        return true;
      },
      err => {
        //error here

        this.error(options.error || err);
        return false;
        //console.log(err);
      }
    );
  }

  customResponse (custom: {text: string, keys: string[]}, res: any) : string {    
    return `${custom.text.split('{}').reduce((prev, curr, i)=> prev += curr + res.data[custom.keys[i-1]])} `;
  }
}
