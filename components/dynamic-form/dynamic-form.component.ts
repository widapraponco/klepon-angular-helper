import { Component, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { HttpClient } from '@angular/common/http';
import { QuestionBase, DropdownQuestion, FileboxQuestion } from '../../models';

@Component({
  selector: "app-question",
  templateUrl: "./dynamic-form.component.html"
})
export class DynamicFormComponent {
  @Input() question: any;
  @Input() form: FormGroup;
  get isValid() {
    return this.form.controls[this.question.key].valid;
  }
  preview: string;
  filename: string;
  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.updateQuestion();
    this.preview = null;
    this.filename = '';
  }

  updateQuestion() {
    let control = this.form.controls[this.question.key];
    // console.log(this.question.key);
    if (this.question instanceof DropdownQuestion) {
      control.setValue(this.question.value || this.question.options[0].key);
    } else {
      control.setValue(this.question.value);
    }
    if (this.question.disabled) {
      control.disable();
    }
  }

  uploadDocument(event: any, name: string) {
    if (event.target.files && event.target.files[0]) {
      this.question = (this.question as FileboxQuestion);
      this.form.controls[name].setValue(event.target.files[0]);
      this.filename = event.target.files[0].name;
      const reader = new FileReader();
      reader.readAsDataURL(this.form.controls[name].value);
      reader.onload = (e: any) => {
        this.preview = e.target.result;
      };
    }
  }

  changeValue(event: any, name: string) {
    if (event.target.value) {
      this.form.controls[name].setValue(event.target.value);
    }
  }
}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
