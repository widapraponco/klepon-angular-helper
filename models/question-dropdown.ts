import { QuestionBase } from './question-base';
import { Observable } from 'rxjs';

export class DropdownQuestion extends QuestionBase<string> {
    public controlType = 'dropdown';
    public options: any;

    constructor(options: {} = {}) {
        super(options);
        this.options = options['options'] || [];
    }

    // async getData() {
    //     console.log(this.options);
        
    //     if (this.options instanceof Observable) {
    //         this.options = this.options.toPromise()
    //             .then()
    //     }
    // }
}