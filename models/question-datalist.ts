import { QuestionBase } from './question-base';
import { Observable } from 'rxjs';

export class DatalistQuestion extends QuestionBase<string> {
    public controlType = 'datalisted';
    public options: any;
    public type: string;

    constructor(options: {} = {}) {
        super(options);
        this.options = options['options'] || [];
        this.type = options['type'] || [];
    }
}