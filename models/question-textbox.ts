import { QuestionBase } from './question-base';

export class TextboxQuestion extends QuestionBase<string> {
    public controlType = 'textbox';
    public type: string;
    public suffix: any;
    public prefix: any;

    constructor(options: {} = {}) {
        super(options);
        this.type = options['type'] || [];
        this.suffix = options['suffix'] || null;
        this.prefix = options['prefix'] || null;
    }
}