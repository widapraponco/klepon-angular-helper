import { FormControl } from '@angular/forms';

export class QuestionBase<T> {
    public value: T;
    public key: string;
    public label: string;
    public required: boolean;
    public order: number;
    public controlType: string;
    public form: FormControl;
    public disabled: boolean;
    public questions: QuestionBase<T>[];
    public suffix: any;
    public prefix: any;
    public options: any;
    public type: string;    
    public loading: boolean = true;
    public inline: boolean;
  
    constructor(options: {
        value?: T,
        key?: string,
        label?: string,
        required?: boolean,
        disabled?: boolean,
        order?: number,
        controlType?: string
      } = {}) {
      this.value = options.value;
      this.key = options.key || '';
      this.label = options.label || '';
      this.required = !!options.required;
      this.disabled = !!options.disabled;
      this.order = options.order === undefined ? 1 : options.order;
      this.controlType = options.controlType || '';
      this.questions = [];
    }

    async getData() {
      //override
    }
  }