import { QuestionBase } from './question-base';

export enum FileType {
    IMAGE=".jpg, .png, .jpeg|image/*",
    PDF="application/pdf",
    CUSTOM=""
}

export class FileboxQuestion extends QuestionBase<string> {
    public controlType = 'filebox';
    public type: string;
    public fileType: string;

    constructor(options: {} = {}) {
        super(options);
        this.type = options['type'] || [];
        this.fileType = FileType[options['fileType'] || FileType.IMAGE];
    }
}