import { QuestionBase } from './question-base';
import { Observable } from 'rxjs';

export class FutureDatalistQuestion extends QuestionBase<string> {
    public controlType = 'future-datalisted';
    public options: any;
    public type: string;
    public loading: boolean = true;

    constructor(options: {} = {}) {
        super(options);
        this.options = options['options'] || [];
        this.type = options['type'] || [];
    }

    async getData() {
        if (this.options instanceof Observable) {
            await this.options.toPromise()
                .then((data)=>this.options = data);
            this.loading = false;        
            this.form.setValue(this.value);
        }
    }
}