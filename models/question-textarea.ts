import { QuestionBase } from './question-base';

export class TextareaQuestion extends QuestionBase<string> {
    public controlType = 'textarea';

    constructor(options: {} = {}) {
        super(options);
    }
}