import { QuestionBase } from './question-base';

export class CheckboxQuestion extends QuestionBase<string> {
    public controlType = 'checkbox';
    public inline: boolean = false;
    public questions: QuestionBase<any>[] = [];

    constructor(options: {} = {}) {
        super(options);
        this.inline = options['inline'] || false;
        this.questions = options['questions'] || [];
    }
}