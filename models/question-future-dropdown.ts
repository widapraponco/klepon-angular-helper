import { QuestionBase } from './question-base';
import { Observable } from 'rxjs';

export class FutureDropdownQuestion extends QuestionBase<string> {
    public controlType = 'future-dropdown';
    public options: any;
    public loading: boolean = true;

    constructor(options: {} = {}) {
        super(options);
        this.options = options['options'] || [];
    }

    async getData() {
        if (this.options instanceof Observable) {
            await this.options.toPromise()
            .then((data)=>this.options = data);
            this.loading = false;        
            this.form.setValue(this.value);
        }
    }
}