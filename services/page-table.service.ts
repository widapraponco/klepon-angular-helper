import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PageTableService {
  // save all data
  dataTable: any[];
  limit: number = 10;
  // temporary data
  pageData: any[];
  filterData: any[];
  maxPage: number;
  minIndex: number = 0;
  constructor() {
  }

  addDataTable(_dataTable: any[]) {
    this.dataTable = _dataTable;
    this.pageData = _dataTable;
    this.filterData = _dataTable;
    this.setTotalPage();
    this.changePage();
  }

  setfilter(e, key) {
    let val = e.target.value;    
    if (val == 'all')  {
      this.pageData = this.dataTable;
    } else {
      this.pageData = this.dataTable.filter(d => d[key] === val);
    }
    this.currentPage = 1;
    this.setTotalPage();
    this.changePage();
  }

  setTextFilter(e, keys) {
    let val: string = e.target.value;
    if (e !== '') {
      var items: any[];
      for(let key of keys) {
        items = this.dataTable.filter(d=> d[key.toLowerCase()].toLowerCase().indexOf(val.toLowerCase()) > -1);
        if (items.length > 0) break;
      }
      
      this.pageData = items;
    } else {
      this.pageData = this.dataTable;
    }
    this.currentPage = 1;
    this.setTotalPage();
    this.changePage();
  }

  setLimitRow(event) {
    this.limit = event.target.value;
    this.changePage();
  }

  setTotalPage() {
    let realNum = Math.floor(this.pageData.length/this.limit);
    this.maxPage = (this.pageData.length/this.limit) > realNum ? realNum+1 : realNum;
    this.maxPage = this.maxPage < 1 ? 1 : this.maxPage;
  }


  currentPage: number = 1;
  prev() {
    this.currentPage--;
    this.changePage();
  }

  next () {
    this.currentPage++;
    this.changePage();
  }

  pagiNation(page: number) {
    this.currentPage = page;
    this.changePage();
  }

  changePage() {
    this.minIndex = this.currentPage*this.limit - this.limit;
    const maxIndex = this.currentPage*this.limit;
    this.filterData = this.pageData.filter((d, i)=>i >= this.minIndex && i < maxIndex);
  }
}
