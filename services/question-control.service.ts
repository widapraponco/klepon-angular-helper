import { Injectable } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { QuestionBase, FileboxQuestion } from '../models';

@Injectable({
  providedIn: 'root'
})
export class QuestionControlService {

  constructor() { }

  toFormGroup(questions: QuestionBase<any>[] ) {
    let group: any = {};
    this.grouping(group, questions)
    return new FormGroup(group);
  }

  grouping(group, questions) {
    questions.map((q)=> this.set(group, q));
  }

  set(group, question) {
    group[question.key] = question.required ? new FormControl(question.value || '', Validators.required)
                                              : new FormControl(question.value || '');
    question.form = group[question.key];
    //handle observable data option;
    question.getData();

    if (question.questions.length > 0) this.grouping(group, question.questions);
  }
}
