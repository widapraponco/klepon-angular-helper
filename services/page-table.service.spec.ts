import { TestBed } from '@angular/core/testing';

import { PageTableService } from './page-table.service';

describe('PageTableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PageTableService = TestBed.get(PageTableService);
    expect(service).toBeTruthy();
  });
});
